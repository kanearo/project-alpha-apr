from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Project
from .forms import ProjectForm


@login_required
def list_projects(request):
    if request.user.is_authenticated:
        projects = Project.objects.filter(owner=request.user)
        context = {"projects": projects}
        return render(request, "projects/list_projects.html", context)
    else:
        return redirect("login")


@login_required
def show_project(request, id):
    project = Project.objects.get(id=id)
    context = {"project": project}
    return render(request, "projects/show_project.html", context)


def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    return render(request, "projects/create_project.html", {"form": form})
