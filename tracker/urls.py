"""tracker URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.shortcuts import redirect
from django.urls import path, include
from accounts.views import login_view, signup
from tasks.views import create_task

urlpatterns = [
    path("admin/", admin.site.urls),
    path("projects/", include("projects.urls", namespace="projects")),
    path("", lambda request: redirect("projects:list_projects"), name="home"),
    path("accounts/login/", login_view, name="login"),
    path("accounts/", include("accounts.urls", namespace="accounts")),
    path("accounts/signup/", signup, name="signup"),
    path("tasks/", include("tasks.urls", namespace="tasks")),
    path("tasks/create/", create_task, name="create_task"),
]
