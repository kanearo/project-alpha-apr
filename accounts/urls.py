from django.urls import path
from .views import logout_view, signup, login_view

app_name = "accounts"

urlpatterns = [
    path("logout/", logout_view, name="logout"),
    path("signup/", signup, name="signup"),
    path("login/", login_view, name="login"),
]
