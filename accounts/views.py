from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from .forms import LoginForm, SignupForm


def logout_view(request):
    logout(request)
    return redirect("login")


def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = LoginForm()
    return render(request, "accounts/login.html", {"form": form})


def signup(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")
            password_confirmation = form.cleaned_data.get(
                "password_confirmation"
            )
            if password != password_confirmation:
                return render(
                    request,
                    "accounts/signup.html",
                    {"form": form, "error": "the passwords do not match"},
                )
            user = User.objects.create_user(
                username=username, password=password
            )
            if user is not None:
                login(request, user)
                return redirect("list_projects")
            else:
                return render(
                    request,
                    "accounts/signup.html",
                    {"form": form, "error": "the passwords do not match"},
                )
    else:
        form = SignupForm()
    return render(request, "accounts/signup.html", {"form": form})
