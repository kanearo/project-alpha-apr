from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .forms import TaskForm
from .models import Task
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("projects:list_projects")
    else:
        form = TaskForm()
    return render(request, "tasks/create.html", {"form": form})


class ShowMyTasksView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/show_my_tasks.html"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)
